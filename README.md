YNot Mail API wrapper.

## Installation ##

Install through composer:

```bash
php -d memory_limit=-1 composer.phar require xlabs/ynotmailbundle
```

In your AppKernel

```php
public function registerbundles()
{
    return [
    	...
    	...
    	new XLabs\YNotMailBundle\XLabsYNotMailBundle(),
    ];
}
```

Add the following command as a crontab to update YNotMail user lists:
```php
55 23 * * * /usr/bin/php <symfony_project_folder>/bin/console cron:daily:ynot:update_lists
```

## Configuration sample
Default values are shown below:
``` yml
# app/config/config.yml

x_labs_ynot_mail:
    api:
        url: 'https://www.ynotmail.com/clients/remote-api.php'
        username: ############
        token: ############
    lists:
        expired_members: {id: xxxx, query: "SELECT * FROM..."}
        alias_2: {id: <list_id_2>, query: "SELECT * FROM..."}
        ...
        alias_N: {id: <list_id_N>, query: "SELECT * FROM..."}
        ...
```

### Usage ###
Instantiate the service from a Controller by:
```php
$api = $this->get('xlabs_ynot_mail');
```
Then use the "_call" method to perform the request, which uses 3 parameters: method, parameters, and multiple flag. If "multiple" is true, it will expect an array of parameters. If the parameter was already an array, it will expect an array of arrays. 

### Methods ###
Create list
```php
$api->_call('createList', array(
    'name' => 'test',
    'ownerEmail' => 'test_owner@mm.com',
    'ownerName' => 'test',
    'replyEmail' => 'test_reply@mm.com',
    'companyName' => 'test',
    'companyAddress' => 'test',
    'companyPhones' => 'test'
));
```
Get lists
```php
$api->_call('getLists');
```
Delete list
```php
$api->_call('deleteList', $list_id);
```
Add subscriber to list
```php
$api->_call('addSubscriberToList', array(
    'listId' => $list_id,
    'subscriber' => array(
        'email' => 'my@email.com',
        'format' => 'html'
    )
));
```
A multiple call to add multiple subscribers to a list would look as follows:
```php
$api->_call('addSubscriberToList', array(
    array(
        'listId' => $list_id,
        'subscriber' => array(
            'email' => 'first@email.com',
            'format' => 'html'
        )
    ),
    array(
        'listId' => $list_id,
        'subscriber' => array(
            'email' => 'second@email.com',
            'format' => 'html'
        )
    )
), $multiple = true);
```

Delete subscriber from list
```php
$api->_call('deleteSubscriber', array(
    'listId' => $list_id,
    'subscriberId' => 'my@email.com'
));
```