<?php

namespace XLabs\YNotMailBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/configuration.html}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('x_labs_y_not_mail');
        $rootNode
            ->children()
                ->arrayNode('api')->isRequired()
                    ->children()
                        ->scalarNode('url')->defaultValue('https://www.ynotmail.com/clients/remote-api.php')->end()
                        ->scalarNode('username')->isRequired()->end()
                        ->scalarNode('token')->isRequired()->end()
                    ->end()
                ->end()
                ->arrayNode('lists')
                    ->useAttributeAsKey('name')
                    ->arrayPrototype()
                        ->children()
                            ->scalarNode('id')->defaultNull()->end()
                            ->scalarNode('query')->defaultNull()->end()
                        ->end()
                    ->end()
                    //->scalarPrototype()->end()
                    /*->children()
                        ->scalarNode('expired_members')->defaultNull()->end()
                    ->end()*/
                ->end()
            ->end()
        ;
        return $treeBuilder;
    }
}
