<?php

namespace XLabs\YNotMailBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Helper\ProgressBar;
use \DateTime;
use MM\UserBundle\Entity\User;
use \Exception;

class UpdateYNotMailListsCommand extends ContainerAwareCommand
{
    private $input;
    private $output;
    private $kernel;
    private $container;
    private $em;
    private $YNOTMailApi;
    private $YNOTMail_config;

    /**
     * Feed YNOT user lists, to send newsletter
     */
    protected function configure()
    {
        $this
            ->setName('cron:daily:ynot:update_lists')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->input = $input;
        $this->output = $output;
        $this->kernel = $this->getApplication()->getKernel();
        $this->container = $this->kernel->getContainer();
        $this->em = $this->container->get('doctrine')->getManager();
        $this->YNOTMailApi = $this->container->get('xlabs_ynot_mail');
        $this->YNOTMail_config = $this->YNOTMailApi->getConfig();

        $YNOTMail_config_lists = $this->YNOTMail_config['lists'];

        foreach($YNOTMail_config_lists as $list)
        {
            $statement = $this->em->getConnection()->prepare($list['query']);
            $statement->execute();
            $users = $statement->fetchAll();
            $this->sendToList($list['id'], $users);
        }
        die;

        // Expired members
        /*if(array_key_exists('expired_members', $YNOTMail_config_lists))
        {
            $qb = $this->em->createQueryBuilder();
            $qb->select('u')
                ->from(User::class, 'u')
                ->where(
                    $qb->expr()->andX(
                        $qb->expr()->orX(
                            $qb->expr()->eq('u.expired', 1), // for when run initially for a site
                            $qb->expr()->lt('u.expiresAt', 'CURRENT_TIMESTAMP()') // for when run initially for a site
                            //$qb->expr()->eq('DATE(u.expiresAt)', 'CURRENT_DATE()')
                            //$qb->expr()->lt('DATE(u.expiresAt)', $qb->expr()->literal((new DateTime())->format('Y-m-d')))
                        ),
                        $qb->expr()->notLike('u.roles', $qb->expr()->literal('%ROLE%'))
                    )
                );
            $q = $qb->getQuery();
            $this->sendToList($YNOTMail_config_lists['expired_members'], $q);
        }

        // Active models
        if(array_key_exists('active_models', $YNOTMail_config_lists))
        {
            $qb = $this->em->createQueryBuilder();
            $qb->select('u')
                ->from(User::class, 'u')
                ->where(
                    $qb->expr()->andX(
                        $qb->expr()->eq('u.enabled', 1),
                        $qb->expr()->eq('u.locked', 0),
                        $qb->expr()->eq('u.approved', 1),
                        $qb->expr()->like('u.roles', $qb->expr()->literal('%MODEL%'))
                    )
                );
            $q = $qb->getQuery();
            $this->sendToList($YNOTMail_config_lists['active_models'], $q);
        }*/
    }

    private function sendToList($list_id, $aUsers)
    {
        $progress = new ProgressBar($this->output, count($aUsers));
        $progress->start();
        $progress->setFormat('%bar% %current%/%max% -- %message%');
        $aItems = array();
        foreach($aUsers as $user)
        {
            $progress->setMessage($user['id']);
            $aItems[] = array(
                'listId' => $list_id,
                'subscriber' => array(
                    'email' => $user['email'],
                    'format' => 'html'
                )
            );
            $progress->advance();
        }
        $this->YNOTMailApi->_call('addSubscriberToList', $aItems, true);
        $progress->finish();
    }

    /*private function sendToList($list_id, $query_users)
    {
        //dump(count($query_users->getArrayResult())); die;
        $progress = new ProgressBar($this->output, count($query_users->getArrayResult()));
        $progress->start();
        $progress->setFormat('%bar% %current%/%max% -- %message%');
        $aUsers = array();
        $count = 0;
        foreach($query_users->iterate() as $user)
        {
            $count++;
            $user = $user[0];
            $progress->setMessage($user->getId());
            $aUsers[] = array(
                'listId' => $list_id,
                'subscriber' => array(
                    'email' => $user->getEmail(),
                    'format' => 'html'
                )
            );
            $progress->advance();
        }
        $this->YNOTMailApi->_call('addSubscriberToList', $aUsers, true);
        $progress->finish();
    }*/
}