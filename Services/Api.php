<?php

namespace XLabs\YNotMailBundle\Services;

use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use XLabs\YNotMailBundle\Services\Commands\Request;
use XLabs\YNotMailBundle\Services\Commands\Response;
use XLabs\YNotMailBundle\Services\Commands\CreateList;
use \Exception;

class Api
{
    protected $encoder;
    protected $normalizer;
    protected $serializer;
    protected $xlabs_ynotmail_config;

    public function __construct($xlabs_ynotmail_config)
    {
        $this->xlabs_ynotmail_config = $xlabs_ynotmail_config;
        $this->encoder = array(new XmlEncoder());
        $this->normalizer = array(new ObjectNormalizer());
        $this->serializer = new Serializer($this->normalizer, $this->encoder);
    }

    public function _call($method, $param = null, $multiple = false)
    {
        $request_type = $this->getRequestType($method);
        if($request_type)
        {
            $request = new Request($method, $param, $request_type, $this->xlabs_ynotmail_config, $multiple);
            $response = $this->send($request);
            // Deserializing the response depending on its size might cause the parser to break
            return $this->getResponse($response, ($multiple && count($param) > 100));
        } else {
            //throw new Exception('Unknown request type for method "'.$method.'"');
            return false;
        }
    }

    private function getRequestType($method)
    {
        $request_type = false;
        switch($method)
        {
            case 'createList':
            case 'getLists':
            case 'deleteList':
            case 'addSubscriberToList':
            case 'bindCustomField':
            case 'listCustomFields':
            case 'createNewsletter':
            case 'sendNewsletter':
            case 'getNewsletter':
            case 'updateNewsletter':
            case 'getNewslettersList':
            case 'getSegments':
            case 'sendNewsletterToSegment':
                $request_type = 'mailing';
                break;
            case 'setConfirmedStatus':
            case 'getConfirmedStatus':
            case 'setActivityStatus':
            case 'getActivityStatus':
            case 'deleteSubscriber':
            case 'updateSubscriber':
            case 'moveSubscriber':
            case 'getSubscribers':
            case 'getSubscriberData':
            case 'checkSuppression':
            case 'suppressEmail':
            case 'unsuppressEmail':
                $request_type = 'subscribers';
                break;
            case 'getNewsletters':
            case 'getNewsletterStats':
                $request_type = 'stats';
                break;
            case 'create':
                $request_type = 'autoresponders';
                break;
        }
        return $request_type;
    }

    public function getConfig()
    {
        return $this->xlabs_ynotmail_config;
    }

    /*public function createList($list)
    {
        $request = new Request('createList', $list, 'mailing', $this->xlabs_ynotmail_config);
        $response = $this->send($request);
        return $this->getResponse($response);
    }

    public function getLists()
    {
        $request = new Request('getLists', null, 'mailing', $this->xlabs_ynotmail_config);
        $response = $this->send($request);
        return $this->getResponse($response);
    }

    public function deleteList($list_id)
    {
        $request = new Request('deleteList', $list_id, 'mailing', $this->xlabs_ynotmail_config);
        $response = $this->send($request);
        return $this->getResponse($response);
    }

    public function addSubscriberToList($list_id, $subscriber)
    {
        $request = new Request('addSubscriberToList', array(
            'listId' => $list_id,
            'subscriber' => $subscriber
        ), 'mailing', $this->xlabs_ynotmail_config);
        $response = $this->send($request);
        return $this->getResponse($response);
    }

    public function deleteSubscriber($list_id, $subscriber_id)
    {
        $request = new Request('deleteSubscriber', array(
            'listId' => $list_id,
            'subscriberId' => $subscriber_id
        ), 'subscribers', $this->xlabs_ynotmail_config);
        $response = $this->send($request);
        return $this->getResponse($response);
    }*/

    public function getXML($data)
    {
        return $this->serializer->serialize($data, 'xml', array(
            'xml_format_output' => true,
            'xml_encoding' => 'UTF-8',
            'xml_root_node_name' => 'xmlrequest'
        ));
    }

    public function getResponse($response, $as_xml = false)
    {
        return $as_xml ? $response : $this->serializer->deserialize($response, Response::class, 'xml', array(
            'as_collection' => true
        ));
    }

    public function send($request)
    {
        if(is_array($request)) // test for multiple at once, still need to be tested, for instance, to speed up adding 500 users
        {
            $aCurl = array();
            foreach($request as $i => $r)
            {
                $data = $this->getXML($request);
                //dump($data);
                $curl = curl_init();
                curl_setopt($curl, CURLOPT_POST, 1);
                curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                $headers = array(
                    "Content-Type: application/xml",
                    "Accept: application/xml",
                );
                curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($curl, CURLOPT_URL, $this->xlabs_ynotmail_config['api']['url']);
                $aCurl[] = $curl;

                if($i%10 == 0)
                {
                    $mh = curl_multi_init();
                    foreach($aCurl as $c)
                    {
                        curl_multi_add_handle($mh, $c);
                    }
                    $active = null;
                    do {
                        $mrc = curl_multi_exec($mh, $active);
                    } while ($mrc == CURLM_CALL_MULTI_PERFORM);

                    while ($active && $mrc == CURLM_OK) {
                        if (curl_multi_select($mh) != -1) {
                            do {
                                $mrc = curl_multi_exec($mh, $active);
                            } while ($mrc == CURLM_CALL_MULTI_PERFORM);
                        }
                    }
                    foreach($aCurl as $c)
                    {
                        curl_multi_remove_handle($mh, $c);
                    }
                    curl_multi_close($mh);
                    $aCurl = array();
                }
            }
        } else {
            $data = $this->getXML($request);
            //dump($data);
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_POST, 1);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
            $headers = array(
                "Content-Type: application/xml",
                "Accept: application/xml",
            );
            curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_URL, $this->xlabs_ynotmail_config['api']['url']);

            $response = curl_exec($curl);
            if(curl_error($curl))
            {
                return curl_error($curl);
            }
            curl_close($curl);
            //dump($response);
            return $response;
        }
    }
}