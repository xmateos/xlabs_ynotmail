<?php

namespace XLabs\YNotMailBundle\Services\Commands;

class CreateList
{
    public $list;
    //private $name;
    //private $ownerEmail;
    //private $ownerName;
    //private $replyEmail;
    //private $companyName;
    //private $companyAddress;
    //private $companyPhones;

    /*
     * $aData = array(
            'name' => 'test',
            'ownerEmail' => 'test_owner@mm.com',
            'ownerName' => 'test',
            'replyEmail' => 'test_reply@mm.com',
            'companyName' => 'test',
            'companyAddress' => 'test',
            'companyPhones' => 'test'
        )
     */
    public function __construct($aData)
    {
        //foreach($aData as $key => $val)
        //{
        //    $this->__set($key, $val);
        //}
        $this->list = $aData;
    }

    public function __get($name)
    {
        return $this->$name;
    }
    public function __set($name, $value)
    {
        $this->$name = $value;
        return $this;
    }
}