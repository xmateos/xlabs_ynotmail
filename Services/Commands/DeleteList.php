<?php

namespace XLabs\YNotMailBundle\Services\Commands;

class DeleteList
{
    public $listId;

    public function __construct($listId)
    {
        $this->listId = $listId;
    }
}