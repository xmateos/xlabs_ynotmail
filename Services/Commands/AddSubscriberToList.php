<?php

namespace XLabs\YNotMailBundle\Services\Commands;

class AddSubscriberToList
{
    public $listId;
    public $subscriber;

    public function __construct($aData)
    {
        $this->listId = $aData['listId'];
        $this->subscriber = $aData['subscriber'];
    }

    public function __get($name)
    {
        return $this->$name;
    }
    public function __set($name, $value)
    {
        $this->$name = $value;
        return $this;
    }
}