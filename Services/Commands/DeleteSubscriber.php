<?php

namespace XLabs\YNotMailBundle\Services\Commands;

class DeleteSubscriber
{
    public $listId;
    public $subscriberId;

    public function __construct($aData)
    {
        $this->listId = $aData['listId'];
        $this->subscriberId = $aData['subscriberId'];
    }
}