<?php

namespace XLabs\YNotMailBundle\Services\Commands;

use \Exception;

class Request
{
    public $username;
    public $usertoken;
    public $requesttype;
    public $requestmethod;
    public $details;

    public function __construct($method, $data = null, $request_type, $config, $multiple)
    {
        $this->username = $config['api']['username'];
        $this->usertoken = $config['api']['token'];
        $this->requesttype = $request_type;
        $this->requestmethod = lcfirst($method);
        $cmd = 'XLabs\YNotMailBundle\Services\Commands\\'.ucfirst($method);
        if(class_exists($cmd))
        {
            if($multiple)
            {
                $this->details = array();
                foreach($data as $item)
                {
                    $this->details[] = new $cmd($item);
                }
            } else {
                $this->details = new $cmd($data);
            }
        } else {
            throw new Exception('Class "'.$cmd.'" not implemented in bundle yet.');
        }
    }

    public function __get($name)
    {
        return $this->$name;
    }
    public function __set($name, $value)
    {
        $this->$name = $value;
        return $this;
    }
}